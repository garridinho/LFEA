#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void r2_modos(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
  TGraphErrors *graph =new TGraphErrors("2_modos.txt","%lg %lg %lg %lg", "");
  graph->SetMarkerStyle(5);
  graph->SetMarkerSize(1.5);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	gStyle->SetOptStat("nermou");
	gStyle->SetOptFit(1000111);

	f2=new TF1("func2","abs(cos((x-[#Delta L'(cm)])/(2*[L(cm)])*pi))*exp(-(x-[#Delta L'(cm)])/[#lambda(cm)])");

//
//--------------------Títulos-------------------------------------
	graph->SetTitle("");

	//V_I
	graph->GetXaxis()->SetTitle("#Delta L(cm)");
	graph->GetYaxis()->SetTitle("K");
	graph->GetYaxis()->SetTitleOffset(1.3);

        f2->SetParameter(0,-2.8);
	f2->SetParameter(1,30);
	f2->SetParameter(2,13);
	graph->Fit(f2);

	graph->Draw("ap");
}
