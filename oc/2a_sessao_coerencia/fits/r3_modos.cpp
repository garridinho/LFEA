#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void r3_modos(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
  TGraphErrors *graph =new TGraphErrors("3_modos_3.txt","%lg %lg %lg %lg", "");
  TGraphErrors *graph1 =new TGraphErrors("3_modos_4.txt","%lg %lg %lg %lg", "");
  //graph->SetMarkerStyle(5);
  //graph->SetMarkerSize(1.5);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	gStyle->SetOptStat("nermou");
	gStyle->SetOptFit(1000111);

	//3 modos
	f2=new TF1("func2","abs(1+2*cos((x-[#Delta L'(cm)])*pi/[L(cm)]))*exp(-(x-[#Delta L'(cm)])/[#lambda(cm)])/3");
	//4 modos
	f3=new TF1("func3","abs(cos(3*(x-[#Delta L'(cm)])*pi/[L(cm)]/2)+cos((x-[#Delta L'(cm)])*pi/[L(cm)]/2))*exp(-(x-[#Delta L'(cm)])/[#lambda(cm)])/2");


//--------------------Títulos-------------------------------------
	graph->SetTitle("");

	//V_I
	graph->GetXaxis()->SetTitle("#Delta L(cm)");
	graph->GetYaxis()->SetTitle("K");
	graph->GetYaxis()->SetTitleOffset(1.3);


	//graph->Fit(f2);

	graph->Draw("ap");
	graph1->Draw("psame");
}
