#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

Double_t myfunction(Double_t *x, Double_t *par)
{
   Float_t xx =x[0]*194.5/4.;
   Float_t uu=sqrt(220*220-xx*xx);
   Double_t nr1=+1./(M_PI)*(2*par[1]*uu+4*par[2]*(uu*xx*xx+uu*uu*uu/3.)+6*par[3]*(uu*xx*xx*xx*xx+2*uu*uu*uu/3.*xx*xx+pow(uu,5)/5.))*0.0307699/(2*M_PI)+0.00026673695; //23ºC
   Double_t t=273.15*0.00029115/nr1-273.15;
   //Double_t t=nr1+1;
   return t;
}

void rvalues(){
  
  TF1*f1 =new TF1("myfunc",myfunction,0,3,4);
  //double a[4]={4.783,-0.0002958,0.00000000655,-5.12E-14};
  //double a[4]={4.673,-0.0003372,0.000000008922,-8.309E-14};
  //double a[4]={4.417,-0.0003314,0.000000009087,-8.731E-14};
  //double a[4]={4.134,-0.0003337,0.000000009604,-9.519E-14};
  //double a[4]={3.999,-0.0003501,0.00000001137,-1.286E-13};
  double a[4]={3.866,-0.0003965,0.00000001543,-2.121E-13};

  //teste não dos fits
  //double a[4]={5.034,-0.0003309,7.939E-9,-6.754E-14};

  //a,c,e,g
  f1->SetParameters((double *)a);
  f1->SetTitle("");	
  f1->GetXaxis()->SetTitle("r (mm)");
  f1->GetYaxis()->SetTitle("Temperatura (^{o}C)");
  //f1->GetYaxis()->SetTitleOffset(1.3);
  cout << f1->Eval(0.85); //AVALIAR TEMP À SUPERFÍCIE
  
  f1->Draw();
}
