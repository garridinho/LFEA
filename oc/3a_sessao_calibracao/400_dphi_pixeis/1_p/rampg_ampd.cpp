#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void rampg_ampd(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
  TGraphErrors **graph=new TGraphErrors*[6];

  string b="1";
  b.append(".txt");
  for(int i=0;i<6;i++)
    {
      graph[i] =new TGraphErrors(b.c_str(),"%lg %lg %lg %lg", "");
      b[0]+=1;
    }
  TF1* f1=new TF1("fa1","[0]+[1]*pow(x-[4],2)+[2]*pow(x-[4],4)+[3]*pow(x-[4],6)",0,640);

  f1->SetParNames("a","c","e","g","x_{0}");
  
  graph[0]->SetMarkerStyle(1);
  graph[0]->SetLineColor(kBlue);

  graph[1]->SetMarkerStyle(1);
  graph[1]->SetLineColor(kBlue);

  graph[2]->SetMarkerStyle(1);
  graph[2]->SetLineColor(kBlue);

  graph[3]->SetMarkerStyle(1);
  graph[3]->SetLineColor(kBlue);

  graph[4]->SetMarkerStyle(1);
  graph[4]->SetLineColor(kBlue);

  graph[5]->SetMarkerStyle(1);
  graph[5]->SetLineColor(kBlue); 
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
  gStyle->SetOptStat("nermou");
  gStyle->SetOptFit(1000111);

   

//--------------------Títulos-------------------------------------
  graph[0]->SetTitle(" ");

	//V_I
  graph[0]->GetXaxis()->SetTitle("n^{o} P#acute{i}xel");
  graph[0]->GetYaxis()->SetTitle("#Delta #phi (rad)");
  graph[0]->GetYaxis()->SetTitleOffset(1.3);


  graph[0]->Draw("ap");
  graph[1]->Draw("ap");
  graph[2]->Draw("ap");
  graph[3]->Draw("ap");
  //graph[4]->Draw("ap");
  //graph[5]->Draw("ap");

}
