#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void ramp(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------


  TGraphErrors *graph1 =new TGraphErrors("1.txt","%lg %lg %lg", " ");
  TGraphErrors *graph =new TGraphErrors("2.txt","%lg %lg %lg", " ");
  graph->SetMarkerSize(1);
 graph->SetLineColor(kBlue);
 graph1->SetLineColor(kRed);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	//gStyle->SetOptStat("nermou");
	//gStyle->SetOptFit(1000111);

//--------------------Títulos------------------------------------

	//V_I
	graph1->GetXaxis()->SetTitle("x (pixeis)");
	graph1->GetYaxis()->SetTitle("y (pixeis)");
	graph1->GetYaxis()->SetTitleOffset(1.3);
        graph1->GetXaxis()->SetRangeUser(0,640);

	graph1->Draw("ap");
	graph->Draw("psame");
}
