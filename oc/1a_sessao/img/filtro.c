#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#define WID 360
#define HGT 360

int main(int argc,char **argv)
{
	FILE *f=fopen("FocoL2.dat","r");
	FILE *o=fopen("out.dat","w");
	FILE *o2=fopen("unfilter.dat","w");
	char buf[200];
	int64_t i;
	int out;
	double x_d,y_d;

	for(i=0;i<WID*HGT;i++){
		x_d = (double)(i % WID) - WID / 2;
		y_d = (double)(i / WID) - HGT / 2;
		fgets(buf,200,f);
		out=atoi(buf);
		if( out > 128 && hypot(x_d,y_d)>100 )
		 fprintf(o,"%f %f %f\n",x_d,y_d,(double)(atoi(buf)/255.0));
	}
	fclose(f);
	fclose(o);
	fclose(o2);

	return 0;
}
