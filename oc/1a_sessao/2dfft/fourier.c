#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fftw3.h>
#include <math.h>
#include <float.h>

#include "fourier.h"
#include "img_help.h"

#define M_PI 3.141592653589793 

void
die(const char *msg)
/* exit while displaying error message */
{
	puts(msg);
	exit(EXIT_FAILURE);
}

void
flip(img_head head, fftw_complex * img)
/* puts fourier transform the right way around 
 * also puts it back the right way for the inverse transform */
{
	uint64_t i, j;
	double temp;
	for (i = 0; i < head.w * head.h / 2; i++) {
		j = ((i % head.w) + head.w / 2) % head.w +
			 (((i / head.w) + head.h / 2) % head.h) * head.w;
		temp = img[j][0];
		img[j][0] = img[i][0];
		img[i][0] = temp;
		temp = img[j][1];
		img[j][1] = img[i][1];
		img[i][1] = temp;
	}
}

void
rect2pol(img_head head, fftw_complex * in, double *abs, double *arg)
/* self explanatory, goes from complex numbers in rectangular coordinates
 * to polar coordinates */
{
	int i;
	for (i = 0; i < head.h * head.w; i++) {
		abs[i] = hypot(in[i][0], in[i][1]);
		arg[i] = atan2(in[i][1], in[i][0]);
	}
}

int fp_rad_bi(double x_d,double y_d,double rad1,double rad2)
/* radial bandstop/bandpass filter, 
 * negative rad -> bandstop, positive rad-> bandpass */
{
	if (rad1 > 0 && rad2 > 0) {
		if (rad1 < hypot(x_d, y_d) && hypot(x_d, y_d) < rad2 ) 
			return 1;
	} else {
		if ( -rad1 > hypot(x_d, y_d) || hypot(x_d, y_d) > -rad2 ) 
			return 1;
	}
	return 0;
}

int fp_rad_sing(double x_d,double y_d,double rad,double notused)
/* radial highpass/lowpass filter, 
 * negative rad -> lowpass, positive rad-> highpass */
{
	(void)notused;
	if (rad > 0) {
		if (hypot(x_d, y_d) < rad) 
			return 1;
	} else {
		if (hypot(x_d, y_d) > -rad)
			return 1;
	}
	return 0;
}

int fp_norm_cond(double x_d,double y_d,double ang,double dist)
/* straight line filter ang is angle of line dist is y intercept
 * kinda glitchy with the 90 degree singularity */
{
	return y_d<tan(-ang*M_PI/180)*x_d-dist;
}

int fp_vert_cond(double x_d,double y_d,double ang,double dist)
{
	(void)y_d;
	return x_d*copysign(1,ang)<dist;
}

void
filter(char filtname,img_head head, fftw_complex *img,double par1,double par2)
{
	int64_t i;
	double x_d, y_d;
   int(*cond)(double,double,double,double);

	switch(filtname) {
		case 'r':
			cond=&fp_rad_bi;
			break;
		case 's':
			cond=&fp_rad_sing;
			break;
		case 'p':
			if (fabs(fabs(par1)-90.0) < 1e-4 ) {
				cond=&fp_vert_cond;
			} else {
				cond=&fp_norm_cond;
			}
			break;
		default:
			die("filter not found");
			break;
	}

	for (i = 0; i < head.w * head.h; i++) {
		x_d = (double)(i % head.w) - head.w / 2;
		y_d = (double)(i / head.w) - head.h / 2;
		if ( (*cond)(x_d,y_d,par1,par2) ) {
			img[i][0] = 0;
			img[i][1] = 0;
		}
	}
	return;
}

int
main(int argc, char **argv)
/* this is main */
{
	FILE *f = fopen(argv[1], "r");
	fftw_plan plan;
	fftw_complex *dft_img;
	fftw_complex *img;
	double *img_abs, *img_arg;
	img_head head;
	char *outstrflt,*outstrinv,*temp;

	if (!f)
		die("file not found");

	if (argc != 7)
		die("wrong # of args\n"
			 "fourier <img> <suf> <filter> <scale1> <filt1> <filt2>\n"
			 "filters: s:lowpass/highpass filt1: radius (negative is lp)\n"
			 "         r:bandpass/bandstop filt1: radius1 filt2:radius2 (neg. is bp)\n"
			 "         p:scliren/plane filt1: angle of plane filt2: distance to 0");

	temp=extsub(argv[1],argv[2]);
	outstrflt=extsub(temp,"flt.pgm");
	outstrinv=extsub(temp,"inv.pgm");
	free(temp);

	head = parse_header(f);

	img = fftw_alloc_complex(head.h * head.w);
	img_abs = malloc(sizeof(double) * head.h * head.w);
	img_arg = malloc(sizeof(double) * head.h * head.w);

	switch (head.type) {
	case '5':
		puts("bin");
		read_bin(f, head, img);
		break;
	case '2':
		puts("ascii");
		read_ascii(f, head, img);
		break;
	default:
		die("Unsupported image type");
	}
	fclose(f);
	
	dft_img = fftw_alloc_complex(head.h * head.w);
	plan = fftw_plan_dft_2d(head.h, head.w,
									img, dft_img, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	flip(head, dft_img);

	d_print_cpx("out.txt",head,dft_img);

	filter(argv[3][0],head, dft_img, atof(argv[5]),atof(argv[6]));

	rect2pol(head, dft_img, img_abs, img_arg);
	write_img(outstrflt, head, img_abs, atof(argv[4]));
   
	flip(head, dft_img);

	plan = fftw_plan_dft_2d(head.h, head.w,
									dft_img, img, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	rect2pol(head, img, img_abs, img_arg);
	write_img(outstrinv, head, img_abs, 0.0);
	//write_img("out_arg.pgm",head,img_arg,NORM_SCA);

	fftw_free(img);
	fftw_free(dft_img);
	free(outstrflt);
	free(outstrinv);
	free(img_abs);
	free(img_arg);
}
