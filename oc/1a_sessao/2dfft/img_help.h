void get_line(char *s, FILE * f);
img_head parse_header(FILE * f);
void read_ascii(FILE * f, img_head head, fftw_complex * img);
void read_bin(FILE * f, img_head head, fftw_complex * img);
void write_img(const char *name, img_head head, double *img, uint64_t log_mul);
void d_print_cpx(const char *file, img_head head, fftw_complex * img);
char *extsub(const char *s, const char *ext);
