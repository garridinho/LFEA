#define BUFSZ 200

typedef struct img_head {
	uint64_t w, h, max;
	char type;
} img_head;
/*w

void die(const char *msg);
void flip(img_head head, fftw_complex * img);
void rect2pol(img_head head, fftw_complex * in, double *abs, double *arg);
int fp_rad_bi(double x_d,double y_d,double rad1,double rad2);
int fp_rad_sing(double x_d,double y_d,double rad,double notused);
int fp_norm_cond(double x_d,double y_d,double ang,double dist);
int fp_vert_cond(double x_d,double y_d,double ang,double dist);
void filter(char filtname,img_head head, fftw_complex *img,double par1,double par2);
