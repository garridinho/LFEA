#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <fftw3.h>
#include <float.h>
#include "fourier.h"

void
get_line(char *s, FILE * f) /*fuck comments */
/* helper for reading lines while automaticly skipping comments
 * may not read whole lines */
{										  	
	do {
		fgets(s, BUFSZ, f);
	} while (s[0] == '#');
	return;
}

img_head
parse_header(FILE * f)
/* parses header of pgm image into a neat struct */
{
	char buf[BUFSZ];
	img_head header;
	char *endptr;
	header.w = header.h = 0;

	get_line(buf, f);
	if (buf[0] != 'P' || (buf[1] != '2' && buf[1] != '5'))
		die("wrong magic");
	header.type = buf[1];

	get_line(buf, f);
	header.w = (uint32_t) strtol(buf, &endptr, 10);
	header.h = (uint32_t) strtol(endptr, &endptr, 10);

	printf("%lux%lu\n", header.w, header.h);
	if (!(header.w || header.h))
		die("invalid dimensions");

	header.max = strtol(buf, &endptr, 10);
	get_line(buf, f);
	printf("%ld\n", header.max = strtol(buf, &endptr, 10));

	return header;
}

void
read_ascii(FILE * f, img_head head, fftw_complex * img)
/* read image data from ascii pgm file */
{
	uint64_t i;
	char buf[BUFSZ];
	char *endptr;
	endptr = buf;

	get_line(buf, f);
	for (i = 0; i < (head.h * head.w); i++) {
		if (*endptr == '\n') {
			get_line(buf, f);
			endptr = buf;
		}
		img[i][0] = strtod(endptr, &endptr);
	}
	return;
}

void
read_bin(FILE * f, img_head head, fftw_complex * img)
/* read image data from binary pgm file */
{
	uint64_t i;
	uint8_t *img_8 = malloc(sizeof(char) * head.w * head.h);
	fread(img_8, sizeof(uint8_t), head.w * head.h, f);

	for (i = 0; i < (head.h * head.w); i++)
		img[i][0] = ((double)img_8[i]) / 255;

	free(img_8);
	return;
}

char
*extsub(const char *s, const char *ext)
{
	char *r;
	int i;

	for (i = 0; s[i] != 0 && s[i] != '.'; i++);
	r = malloc(i + 1 + strlen(ext));

	strcpy(r, s);
	strcpy(r + i, ext);

	return r;
}

void
write_img(const char *name, img_head head, double *img, uint64_t log_mul)
/* writes array of double to image with given name, auto scales 
 * data to image intensity resolution can select log scale "amplification"
 * looks like contrast setting? if zero linear scale is used */
{
	FILE *f;
	uint64_t i;
	uint8_t *out = malloc(sizeof(uint8_t) * head.w * head.h);
	double min, max;
	min = DBL_MAX;
	max = -DBL_MAX;

	for (i = 0; i < head.h * head.w; i++) {
		if (img[i] > max)
			max = img[i];
		if (img[i] < min)
			min = img[i];
	}
	printf("min:%f \nmax:%f\n", min, max);

	if (log_mul) {
		for (i = 0; i < head.w * head.h; i++) {
			out[i] = (uint8_t) (255 / log_mul * log2((img[i] - min) / (max - min) *
																  (exp2(log_mul) - 1) + 1));
		}
	} else {
		for (i = 0; i < head.w * head.h; i++)
			out[i] = (uint8_t) (255 * (img[i] - min) / (max - min));
	}

	f = fopen(name, "w");
	fprintf(f, "P5\n%lu %lu\n%lu\n", head.w, head.h, head.max);
	fwrite(out, sizeof(uint8_t), head.w * head.h, f);
	fclose(f);
	free(out);
	return;
}

void
d_print_cpx(const char *file, img_head head, fftw_complex * img)
/* mostly for debug, will print x,y,re,im,abs,rad to a file */
{
	uint64_t i;
	FILE *f = fopen(file, "w");
	fprintf(f, "#w:%lu h:%lu\n#x y re im abs ang", head.w, head.h);
	for (i = 0; i < head.w * head.h; i++)
		fprintf(f, "%lu %lu %f% f% f %f\n", i % head.w, i / head.w,
				img[i][0], img[i][1], 
				hypot(img[i][1],img[i][2]),atan2(img[i][2],img[i][1]));
	fclose(f);
	return;
}
