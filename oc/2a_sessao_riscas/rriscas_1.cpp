#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void rriscas_1(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
  TGraphErrors *graph =new TGraphErrors("riscas_1.txt","%lg %lg %lg", " ");
  graph->SetMarkerStyle(5);
  graph->SetMarkerSize(1.5);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	gStyle->SetOptStat("nermou");
	gStyle->SetOptFit(1000111);

	f2=new TF1("func2","(1+cos((x-[x_{0}])*2*pi/[#Delta x]))/2");

//
//--------------------Títulos-------------------------------------
	graph->SetTitle("");

	//V_I
	graph->GetXaxis()->SetTitle("x (mm)");
	graph->GetYaxis()->SetTitle("I/I_{max}");
	graph->GetYaxis()->SetTitleOffset(1.3);


	graph->Fit(f2);

	graph->Draw("ap");
}
