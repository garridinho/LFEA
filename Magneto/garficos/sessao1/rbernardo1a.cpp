#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void rbernardo1a(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
  TGraphErrors *graph =new TGraphErrors("bernardo1a.txt","%lg %lg %lg %lg", "");
  graph->SetMarkerStyle(5);
  graph->SetMarkerSize(1.5);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	gStyle->SetOptStat("nermou");
	gStyle->SetOptFit(1000111);

	f2=new TF1("func2","[a]*cos([b]*x*x)+[c]");

//--------------------Títulos-------------------------------------
	graph->SetTitle("");
        //V_H
	graph->GetXaxis()->SetTitle("H(Oe)");
	graph->GetYaxis()->SetTitle("V(mV)");
	graph->GetYaxis()->SetTitleOffset(1);

	f2->SetParameters(0.08,0.1,5.26);
	//graph->Fit(f2);

	graph->Draw("ap");
}
