#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

void rVH_faceA_cos2(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
 TGraphErrors *graph1 =new TGraphErrors("RH_faceA_ed.txt","%lg %lg %lg %lg", "");
 TGraphErrors *graph =new TGraphErrors("RH_faceA_de.txt","%lg %lg %lg %lg", "");
 graph->SetMarkerStyle(5);
 graph->SetMarkerSize(1.5);
 //graph->SetMarkerSize(0.5);
 graph1->SetLineColor(kRed);
 graph->SetLineColor(kBlue);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	gStyle->SetOptStat("nermou");
	gStyle->SetOptFit(1000111);

	f2=new TF1("func2","[a]*cos([c]*x-[b])*cos([c]*x-[b])+[d]");

//--------------------Títulos-------------------------------------
	graph->SetTitle("");

	//V_I
	graph->GetXaxis()->SetTitle("H(Oe)");
	graph->GetYaxis()->SetTitle("R(#Omega)");
	graph->GetYaxis()->SetTitleOffset(1.3);
        /*
	f2->SetParameter(1,3);
	f2->SetParameter(0,0.5);
	graph->Fit(f2);
	*/
	graph->Draw("ap");
	graph1->Draw("psame");
}
