#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void rGMR_SV1_400(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
  TGraphErrors *graph =new TGraphErrors("GMR_SV1_400.txt","%lg %lg %lg %lg", "");
  graph->SetMarkerStyle(4);
  graph->SetMarkerSize(0.5);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	gStyle->SetOptStat("nermou");
	gStyle->SetOptFit(1000111);

	f2=new TF1("func2","[R(#Omega)]*x+[b(mV)]");

//--------------------Títulos-------------------------------------
	graph->SetTitle("");

	//V_I
	graph->GetXaxis()->SetTitle("H (Oe)");
	graph->GetYaxis()->SetTitle("MR (%)");
	graph->GetYaxis()->SetTitleOffset(1.3);

	f2->SetParameter(1,1);
	f2->SetParameter(0,1);
	//graph->Fit(f2);

	graph->Draw("ap");
}
