#!/bin/sh

pwd=$(pwd)
echo $1
file=$(ls | grep "$1".*csv$)

for i in $file; do
	sed -e  "s/FICHEIRO/"$i"/g"  mac.mac > mac2.mac
	maxima -b "mac2.mac"
	cd ~/.maxima
	sed  -e 's/lt 1/lt 2 ps 0.3/g' \
	     -e '10aset mxtics 5' \
	     -e '10aset mytics 5' \
	     -i gnuplot.gp ; gnuplot gnuplot.gp
	mv "$i".pdf $pwd
	cd $pwd
done
