#!/bin/sh

pwd=$(pwd)
echo $1
file=$(ls | grep "$1".*csv$)
echo $file

for i in $file; do
	sed -e  "s/FICHEIRO/"$i"/g"  mac3.mac > mac4.mac
	maxima -b "mac4.mac"
	cd ~/.maxima
	sed  -e 's/lt 1/lt 2 ps 0.3/g' \
	     -e '10aset mxtics 5' \
	     -e '10aset mytics 5' \
	     -i gnuplot.gp ; gnuplot gnuplot.gp
	mv "$i".pdf "$pwd"/diff"$i".pdf
	cd $pwd
done
