#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;

void rR_H_SIL_2(){

  gStyle->SetOptFit();
  TCanvas* c1 = new TCanvas("c1","");
  c1->SetGrid();
//--------------------------------------------------------------------

	//NOTA IMP
	// w\o delimiter = x y ey
	// w\  delimiter = x y ex  
//--------------------Ficheiros a ler-----------------------------
 TGraphErrors *graph1 =new TGraphErrors("R_H_SIL_e_d_2.txt","%lg %lg %lg %lg", "	");
 TGraphErrors *graph =new TGraphErrors("R_H_SIL_d_e_2.txt","%lg %lg %lg %lg", "	");
 graph->SetMarkerStyle(1);
 //graph->SetMarkerSize(0.5);
 graph1->SetLineColor(kRed);
 graph->SetLineColor(kBlue);
//----------------------------------------------------------------  

/*********************************************************************/
	//comando da caixinha//
	gStyle->SetOptStat("nermou");
	gStyle->SetOptFit(1000111);

//--------------------Títulos-------------------------------------
	graph->SetTitle("");

	//V_I
	graph->GetXaxis()->SetTitle("H (Oe)");
	graph->GetYaxis()->SetTitle("R (#Omega)");
	graph->GetYaxis()->SetTitleOffset(1.3);

	graph->Draw("ap");

graph1->Draw("psame");



}
