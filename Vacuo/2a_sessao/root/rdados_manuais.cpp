#include <iostream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cstdio>
#include <iomanip>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH3.h>
#include <TF1.h>
#include <TFormula.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TVector.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TChain.h>

using namespace std;
void rdados_manuais()
{
   gStyle->SetOptFit();
   TCanvas *c1 = new TCanvas("c1","multigraph",700,500);
   c1->SetGrid();
      // draw a frame to define the range
   TMultiGraph *mg = new TMultiGraph();
      // create first graph
   TGraphErrors *gr1 =new TGraphErrors("rotativa.txt","%lg %lg %lg %lg", "");
   
   gr1->SetMarkerColor(kBlue);
   gr1->SetMarkerStyle(21);
   gr1->SetMarkerSize(0.5);
   gr1->GetXaxis()->SetTitle("t(s)");
   gr1->GetYaxis()->SetTitle("log(P/P_{0})");
   mg->Add(gr1);
      // create second graph
   TGraphErrors *gr2 =new TGraphErrors("TM_1.txt","%lg %lg %lg %lg", "");

   gr2->SetMarkerColor(kRed);
   gr2->SetMarkerStyle(21);
   gr2->SetMarkerSize(0.5);
   mg->Add(gr2);

      // create second graph
   TGraphErrors *gr3 =new TGraphErrors("TM_2.txt","%lg %lg %lg %lg", "");

   gr3->SetMarkerColor(kBlack);
   gr3->SetMarkerStyle(21);
   gr3->SetMarkerSize(0.5);
     gr3->GetXaxis()->SetTitle("t(s)");
   gr3->GetYaxis()->SetTitle("log(P/P_{0})");
   mg->Add(gr3);
   mg->GetXaxis()->SetTitle("t(s)")
   
   mg->Draw("ap");
     //force drawing of canvas to generate the fit TPaveStats
   c1->Update();
   //TPaveStats *stats1 = (TPaveStats*)gr1->GetListOfFunctions()->FindObject("stats");
   //TPaveStats *stats2 = (TPaveStats*)gr2->GetListOfFunctions()->FindObject("stats");
   //TPaveStats *stats3 = (TPaveStats*)gr3->GetListOfFunctions()->FindObject("stats");
   
   //stats1->SetTextColor(kBlue);
   //stats2->SetTextColor(kRed);
   //stats1->SetX1NDC(0.12); stats1->SetX2NDC(0.32); stats1->SetY1NDC(0.75);
   //stats2->SetX1NDC(0.72); stats2->SetX2NDC(0.92); stats2->SetY1NDC(0.78);
   //c1->Modified();
}
